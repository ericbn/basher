basher
=======

A simple bash-like prompt for [ZIM](https://github.com/zimfw/zimfw), the ZSH-IMproved shell. This prompt is totaly based on the [`gitster` prompt](https://github.com/zimfw/gitster) developped by [Eric Nielsen](https://github.com/ericbn).

